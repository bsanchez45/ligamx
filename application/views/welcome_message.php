<link rel="stylesheet" href="assets/css/mdb.min.css" />
<body>
    <br>
    <div class="container">
        <div class="row">
            <div class="p-5 mb-4 bg-light rounded-3 bg-glass">
                <div class="container">
                <h3><b>Juegas contra: </b></h3>
                    <div class="col-md-12 col-12">
                    <ul class="list-group list-group-light">
                            <li class="list-group-item d-flex justify-content-between align-items-start">
                                <div class="col-xl-5 col-lg-5 col-5">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center">
                                                <img src="https://i.pinimg.com/originals/d6/c7/a4/d6c7a4f3eb6202411029971966b82542.png" alt="" style="width: 45px; height: 45px" class="rounded-circle" />
                                                <div class="ms-3">
                                                    <p class="fw-bold mb-1">America FC</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-2 col-lg-2 col-2">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center">
                                                <h2 class="text-center"><b class="text-center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; VS</b></h2>
                                                <p></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-5 col-lg-5 col-5">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center">
                                                <img src="https://seeklogo.com/images/C/chivas-rayadas-del-guadalajara-1987-logo-8546D0E649-seeklogo.com.png" alt="" style="width: 45px; height: 45px" class="rounded-circle" />
                                                <div class="ms-3">
                                                    <p class="fw-bold mb-1">Chivas FC</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            </ul>
                    </div>
                    <div class="col-md-8 col-8">
                        <br>
                        <h3 class="fw-bold">Bienvenido</h3>
                        <p class="col-md-8 text-danger"><b>Has sido registrado en la liga MX, es recomendarble revisar su correo.</b></p>
                        <a class="btn btn-primary btn-lg" type="button" href="<?php echo base_url('/Usuario_Jugador/Asignacion') ?>" style="background-color: #7e3763;">Generar Asignación  &nbsp;&nbsp;&nbsp;<i class="fa-regular fa-file"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</body>
<style>
    body {
        background-image: url("https://cloudfront-us-east-1.images.arcpublishing.com/infobae/QA7FJNPVBNBMZLDHVJ3S5UYM4Q.jpg");
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center center;
    }

    .bg-glass {
        background-color: hsla(0, 0%, 100%, 0.9) !important;
        backdrop-filter: saturate(300%) blur(25px);
    }
</style>