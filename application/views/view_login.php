
<body class="background-radial-gradient overflow-hidden">
  <!-- Section: Design Block -->
  <div class="container px-4 py-5 px-md-5 text-center text-lg-start my-5">
    <div class="row gx-lg-5 align-items-center mb-5">
      <div class="col-lg-6 mb-5 mb-lg-0" style="z-index: 10">
        <h2 class="fw-bold ls-tight" style="color: hsl(218, 81%, 95%)">
          Primera División de México<br />
          <span style="color: green">Liga de fútbol</span>
        </h2>
        <p class="mb-3 opacity-70" style="color: hsl(218, 81%, 85%)">
          Reconocida a la vez por la IFFHS como la liga más fuerte siglo xxi en la Concacaf y el mundo.
        </p>
      </div>

      <div class="col-lg-6 mb-5 mb-lg-0 position-relative">
        <div id="radius-shape-1" class="position-absolute rounded-circle shadow-5-strong"></div>
        <div id="radius-shape-2" class="position-absolute shadow-5-strong"></div>
        <div id="radius-shape-3" class="position-absolute shadow-5-strong"></div>

        <div class="card bg-glass">
          <div class="card-body px-4 py-5 px-md-5">
            <h3 class="mb-5 text-center"><b>Bienvenido</b></h3>
            <form novalidate method="post" action="<?php echo base_url('Home') ?>" enctype="multipart/form-data">
              <!-- Email input -->
              <div class="form-outline mb-4">
                <input type="text" id="usuario" name="usuario" class="form-control" />
                <label class="form-label" for="usuario">Usuario</label>
              </div>

              <!-- Password input -->
              <div class="form-outline mb-4">
                <input type="password" id="password" name="password" class="form-control" />
                <label class="form-label" for="password">Contraseña</label>
              </div>
              <div class="d-grid gap-2">
                <button class="btn btn-primary" type="submit" style="background-color: #072146;">Iniciar sesión</button>
              </div>
              <div class="d-grid gap-2">
                <a class="btn btn-primary" href="<?php echo base_url('/Usuario_Tecnico') ?>" type="button" style="background-color: #072146;">Tecnico</a>
              </div>
              <div class="d-grid gap-2">
                <a class="btn btn-primary" href="<?php echo base_url('/Usuario_Jugador') ?>" type="button" style="background-color: #072146;">Usuarios</a>
              </div>
              <hr>
              <!-- Register buttons -->
              <div class="text-center">
                <p><b>Liga BBVA MX</b></p>
              </div>
            </form>
          </div>
          <!-- Pills content -->
        </div>
      </div>
    </div>
  </div>
</body>
<style>
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  body {
    font-family: 'Poppins', sans-serif;
    overflow: hidden;
  }

  .background-radial-gradient {
    background-color: hsl(215, 90%, 15%);
    background-image: radial-gradient(650px circle at 0% 0%,
        hsl(215, 90%, 35%) 15%,
        hsl(215, 90%, 30%) 35%,
        hsl(215, 90%, 20%) 75%,
        hsl(215, 90%, 19%) 80%,
        hsl(215, 90%, 19%) 80%,
        transparent 100%),
      radial-gradient(1250px circle at 100% 100%,
        hsl(215, 90%, 45%) 15%,
        hsl(215, 90%, 30%) 35%,
        hsl(215, 90%, 20%) 75%,
        hsl(215, 90%, 19%) 80%,
        transparent 100%);
  }

  #radius-shape-1 {
    height: 220px;
    width: 220px;
    top: -60px;
    left: -130px;
    background-image: radial-gradient(circle at 50% -20.71%, #e16b32 0, #db5b2f 16.67%, #d3462a 33.33%, #c72923 50%, #bb001e 66.67%, #b0001d 83.33%, #a8001f 100%);
    overflow: hidden;
  }


  #radius-shape-2 {
    border-radius: 38% 62% 63% 37% / 70% 33% 67% 30%;
    bottom: -60px;
    right: -110px;
    width: 300px;
    height: 300px;
    background-image: radial-gradient(circle at 50% -20.71%, #9c9e3a 0, #8a962d 16.67%, #748a1c 33.33%, #5a7d00 50%, #3f7000 66.67%, #216500 83.33%, #005c00 100%);
    overflow: hidden;
  }

  .bg-glass {
    background-color: hsla(0, 0%, 100%, 0.9) !important;
    backdrop-filter: saturate(200%) blur(25px);
  }

  #radius-shape-3 {
    border-radius: 58% 92% 89% 50% / 70% 53% 87% 90%;
    bottom: -100px;
    right: 1310px;
    width: 300px;
    height: 300px;
    background-image: linear-gradient(135deg, #505b86 0, #022e63 50%, #000042 100%);
    overflow: hidden;
  }
</style>