<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-info p-3" style="background:#072146 !important;">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" style="background:#fff !important;" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon" style="color:#000 !important;"></span>
        </button>
        <div class=" collapse navbar-collapse" id="navbarNavDropdown">
            <a class="navbar-brand mt-2 mt-lg-0" href="#">
                <img src="https://upload.wikimedia.org/wikipedia/commons/0/07/LIGA_MX.png" height="40" alt="MDB Logo" loading="lazy" />
            </a>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link mx-2 active" id="nav" aria-current="page" href="<?php echo base_url('/Equipo') ?>" style="color: #fff;"><b>Equipos</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mx-2" id="nav" href="<?php echo base_url('/Usuarios') ?>" style="color: #fff;"><b>Usuarios</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mx-2" id="nav" href="<?php echo base_url('/Jornada') ?>" style="color: #fff;"><b>Jornadas</b></a>
                </li>
            </ul>
            <ul class="navbar-nav ms-auto d-none d-lg-inline-flex">
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('/') ?>" id="nav" style="color: #fff;"><b>Cerrar Sesión</b></a>
                </li>
            </ul>
        </div>
        <!-- Collapsible wrapper -->


        <!-- Right elements -->
    </div>
    <!-- Container wrapper -->
</nav>
<!-- Navbar -->
<style>
    #nav {
        text-decoration: none;
        background-image: linear-gradient(currentColor, currentColor);
        background-position: 0% 100%;
        background-repeat: no-repeat;
        background-size: 0% 2px;
        transition: background-size .3s;
    }

    #nav:hover,
    #nav:focus {
        background-size: 100% 2px;
        color: #fff;
    }
</style>