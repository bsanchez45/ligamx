<link rel="stylesheet" href="assets/css/mdb.min.css" />
<br>
<h4 class="text-center"><b><span class="badge badge-light">Resultados de mi equipo</span></b></h4>
<br>
<div class="container">
    <div class="row text-center">
        <div class="col-xl-5 col-lg-5 mb-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <img src="https://i.pinimg.com/originals/d6/c7/a4/d6c7a4f3eb6202411029971966b82542.png" alt="" style="width: 45px; height: 45px" class="rounded-circle" />
                        <div class="ms-3">
                            <p class="fw-bold mb-1">America FC</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-2 col-lg-2">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <h2 class="text-center"><b class="text-center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2 - 1</b></h2>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-5 col-lg-5 mb-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <img src="https://seeklogo.com/images/C/chivas-rayadas-del-guadalajara-1987-logo-8546D0E649-seeklogo.com.png" alt="" style="width: 45px; height: 45px" class="rounded-circle" />
                        <div class="ms-3">
                            <p class="fw-bold mb-1">Chivas FC</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-5 col-lg-5 mb-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <img src="https://i.pinimg.com/originals/d6/c7/a4/d6c7a4f3eb6202411029971966b82542.png" alt="" style="width: 45px; height: 45px" class="rounded-circle" />
                        <div class="ms-3">
                            <p class="fw-bold mb-1">America FC</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-2 col-lg-2">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <h2 class="text-center"><b class="text-center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0 - 2</b></h2>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-5 col-lg-5 mb-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Escudo_del_Cruz_Azul_AC.svg/2048px-Escudo_del_Cruz_Azul_AC.svg.png" alt="" style="width: 45px; height: 45px" class="rounded-circle" />
                        <div class="ms-3">
                            <p class="fw-bold mb-1">Cruz Azul FC</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-5 col-lg-5 mb-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <img src="https://i.pinimg.com/originals/d6/c7/a4/d6c7a4f3eb6202411029971966b82542.png" alt="" style="width: 45px; height: 45px" class="rounded-circle" />
                        <div class="ms-3">
                            <p class="fw-bold mb-1">America FC</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-2 col-lg-2">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <h2 class="text-center"><b class="text-center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4 - 3</b></h2>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-5 col-lg-5 mb-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/a/a2/CF_Monterrey_A11.PNG" alt="" style="width: 45px; height: 45px" class="rounded-circle" />
                        <div class="ms-3">
                            <p class="fw-bold mb-1">Monterrey FC</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>