<link rel="stylesheet" href="assets/css/mdb.min.css" />
<br>
<div class="container">
    <div class="row">
        <div class="card card-cascade narrower border border-light">
            <div class="card-header py-2 d-flex justify-content-between align-items-center" style="background: #bd191b !important; color: #fff;">
                <div class="row">
                    <h2>&nbsp;&nbsp;Gestionar Equipos</h2>
                </div>
                <div class="form-outline col-sm-4">
                </div>
                <div>
                    <!-- Button trigger modal -->
                    <a type="button" href="" class="btn btn-outline-light btn-rounded" data-bs-toggle="modal" data-bs-target="#exampleModal" data-mdb-toggle="tooltip" data-mdb-placement="bottom" style="background-image: radial-gradient(circle at 50% -20.71%, #505467 0, #1f3259 50%, #00154b 100%); color: #fff;" title="Agregar empleado">
                        <i class="fa-sharp fa-solid fa-address-card"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="px-4">
                    <div class="table-wrapper">
                        <table id="myTable">
                            <thead class="bg-light">
                                <tr style="color: #072146;">
                                    <th scope="col">ID</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Fotografia</th>
                                    <th scope="col">CV</th>
                                    <th scope="col">Puesto</th>
                                    <th scope="col">Fecha de ingreso</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td scope="row">1</td>
                                    <td scope="row">Jonathan Jesus</td>
                                    <td>foto.png</td>
                                    <td>Curriculum</td>
                                    <td>Delantero</td>
                                    <td>2019/10/02</td>
                                    <td>
                                        <a href="" role="button" class="btn btn-outline btn-floating" style=" color: #072146; border: 1px solid #072146;" data-mdb-toggle="tooltip" data-mdb-placement="bottom" title="Editar" data-mdb-ripple-color="dark">
                                            <i style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);" class="fa-solid fa-pen-to-square"></i>
                                        </a>
                                        <a href="" role="button" class="btn btn-outline btn-floating" style=" color: #072146; border: 1px solid #072146;" data-mdb-toggle="tooltip" data-mdb-placement="bottom" title="Eliminar" data-mdb-ripple-color="dark">
                                            <i style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);" class="fa-solid fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer text-center" style="background: #767678;">
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Datos del Equipo</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Email address</label>
                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label">Example textarea</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#myTable").DataTable({
            "pageLength": 4,
            lengthMenu: [
                [4, 10, 25, 50],
                [4, 10, 25, 50]
            ],
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.13.1/i18n/es-ES.json",
            }
        });
    });
</script>