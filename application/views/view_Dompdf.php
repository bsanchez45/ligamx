<body style="background-image:url('https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/34/1c/7f/341c7fa1-8ecc-6965-6444-0ff315ee9622/GxAppIcon-0-0-1x_U007emarketing-0-0-0-7-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/1200x630wa.png');
    background-repeat: no-repeat;  background-position: center center; background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-1">
                <label for=""></label>
            </div>
            <div class="col-10">
                <div class="row">
                    <p style="text-align: right;font-weight: bold; color:#072146;">Ciudad de Mexico, Mx., </p>
                    <p style="font-weight: bold; color:#072146;">TECNICO MIGUEL HERRERA &nbsp; </p>

                    <p style="font-weight: bold; color:#072146;">JEFE DE LA FEDERACION MEXICANA</p>

                    <p style="font-weight: bold; color:#072146;">LIGA MX BBVA BANCOMER SIENTE TU LIGA S.A</p>

                    <p style="font-weight: bold; color:#072146;">P R E S E N T E.</p>

                    <p style="font-weight: bold; text-align: center; color:#072146;">ASIGNACION DE FEDERACIÓN</p>

                    <p>Por este conducto me permito presentar a C. <strong>[NOMBRE COMPLETO]</strong> que resientemente forma parte del <strong>[FEDERACION AMERICANISTA FC]</strong> 
                    contando con un alto rendimeinto para el juego com la <strong>[POSICION]</strong> siendo un gran elemento para esta clausura 2023. <strong>[CURP]</strong> durante el período del 09 de
                        enero de 2023 al 21 de abril de 2023, cubriendo hasta la jornada 11 de la LIGA MX</p>

                    <p>Lo anterior es con el objeto de consolidar la formación práctica del jugador, ejerciendo las funciones propias de
                        la <strong>[FEDERACION AMERICANISTA FC]</strong>. Así mismo informo a usted que recibio un correo para verificar que sea usted <strong>[CORREO ELECTRONICO]</strong>
                        cuenta con un Seguro Facultativo del IMSS número 27159818916 proporcionado por el club y sin costo para
                        su salud.</p>

                    <p>
                        Agradeciendo de antemano la atención que se sirva prestar a la presente, sin otro particular por el momento,
                        aprovecho la oportunidad para enviarle un cordial saludo.
                    </p>

                    <br>

                    <p style="font-weight: bold; color:#072146;">ATENTAMENTE</p>
                    <p style="font-weight: bold; color:#072146;"> M.A. MIKEL ANDONI ARRIOLA PEÑALOSA</p>
                    <p style="font-weight: bold; color:#072146;">JEFE DE LA LIGA MX SPORT POR FIFA</p>
                    <img src="https://riskperu.com/wp-content/uploads/2021/10/frima-miguel-min.png" alt="firma" style="width:30%; height:15%;">
                    <br><br>
                    <small style="color:#072146;">La Primera División de México también conocida simplemente como Liga MX o por motivos de patrocinio Liga BBVA MX, ​​​ es la máxima categoría masculina del sistema de ligas de México y la principal competición de clubes del país.Desde el año 2012 cuenta con una organización autónoma respecto a la Federación Mexicana de Fútbol. https://ligamx.net/cancha/tablas/tablaGeneralClasificacion/sp/8934b8c89a62e0</small><br><br>
                    <hr style="background:#072146; height: 2px; width: 70%;">
                    <hr style="background:#072146; border-radius: 300px/100px; height: 2%; text-align: center;">


                </div>
            </div>
            <div class="col-1">
                <label for=""></label>
            </div>
        </div>
    </div>
</body>

</html>

<?php
$HTML = ob_get_clean();
require_once("./application/libraries/autoload.inc.php");

use Dompdf\Dompdf;

$dompdf = new Dompdf();
$opciones = $dompdf->getOptions();
$opciones->set(array("isRemoteEnabled" => true));
$dompdf->setOptions($opciones);

$dompdf->loadHtml($HTML);

$dompdf->setPaper('letter');
$dompdf->render();
$dompdf->stream("archivo.pdf", array("Attachment" => false));
?>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>