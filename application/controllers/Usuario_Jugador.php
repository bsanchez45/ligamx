<?php

class Usuario_Jugador extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Perfiles_model');
    }


    public function index()
    {
        $this->load->view('template/head');
        $this->load->view('template/navbar_jugador');
        $this->load->view('welcome_message');
        $this->load->view('template/footer');
    }
    public function Asignacion(){
        $this->load->view('view_Dompdf');
    }
}