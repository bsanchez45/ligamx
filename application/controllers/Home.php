<?php
class Home extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		//aqui cargamos nuetro modelo
		$this->load->model('Welcome_model');
		// include 
	}


	public function index()
	{
		$this->load->view('template/head');
		$this->load->view('template/navbar');
		$this->load->view('home');
		$this->load->view('template/footer');
	}
}