<?php

class Usuario_Tecnico extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Perfiles_model');
    }


    public function index()
    {
        $this->load->view('template/head');
        $this->load->view('template/navbar_tecnico');
        $this->load->view('home');
        $this->load->view('template/footer');
    }
}